Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: lmod
Source: https://github.com/TACC/Lmod

Files: *
Copyright: 2008-2018 Robert McLay <mclay@tacc.utexas.edu>
License: MIT

Files: shells/Rc.lua
Copyright: 2008-2018 Robert McLay <mclay@tacc.utexas.edu>
           2021 Oak Ridge National Laboratory
License: MIT

Files: tools/base64.lua
Copyright: 2006-2008 Alex Kloss
License: LGPL-2

Files: tools/json.lua
Copyright: 2005-2009 Craig Mason-Jones
License: MIT

Files: tools/deepcopy.lua
Copyright: 2012 Declan White
License: MIT

Files: pkgs/luafilesystem/*
Copyright: 2003-2020 Kepler Project
License: MIT

Files: pkgs/term/*
Copyright: 2009 Rob Hoelz <rob@hoelzro.net>
License: MIT

Files: contrib/tracking_module_usage/conf_create
       contrib/tracking_module_usage/progressBar.py
Copyright: (C) 2013-2014 University of Texas at Austin
           (C) 2013-2014 University of Tennessee
License: LGPL-2.1+

Files: hermes/*
Copyright: 2008-2014 Robert McLay <mclay@tacc.utexas.edu>
License: MIT

Files: debian/*
Copyright: 2014 Aaron Zauner <azet@azet.org>
           2021 Alexandre Strube <surak@surak.eti.br>
 © 2022-2025 Andreas Beckmann <anbe@debian.org>
License: MIT

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.

License: LGPL-2
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Library General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 .
 You should have received a copy of the GNU Library General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 .
 On Debian systems, the full text of the GNU Library General
 Public License version 2 can be found in the file
 `/usr/share/common-licenses/LGPL-2'.

License: LGPL-2.1+
 This package is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License can be found in "/usr/share/common-licenses/LGPL-2.1".
